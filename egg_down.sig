#Egg Download

#signature bothunter_http{
#	  ip-proto == tcp
#	  src-port >= 1028
#	  dst-port <= 1040
#	  payload /(?=.*GET)(?=.*HTTP)(?=.*.exe)/ 
#	  event "E3[rb] BotHunter HTTP-based .exe Upload on backdoor port"
#}

signature bothunter_http{
          ip-proto == tcp
          src-port >= 1028
          src-port <= 1040
          #src-port <= 65535
          payload /GET/
	  requires-signature bothunter_http2
          requires-signature bothunter_http3
          event "E3[rb] BotHunter HTTP-based .exe Upload on backdoor port"
}
signature bothunter_http2{
          ip-proto == tcp
          src-port >= 1028
          src-port <= 1040
          #src-port <= 65535
          payload /HTTP/
}
signature bothunter_http3{
          ip-proto == tcp
          src-port >= 1028
          src-port <= 1040
          #src-port <= 65535
          payload /.*\.exe/
}


signature bleeding_edge{
          ip-proto == tcp
	  src-port != 20
          payload /.*MZ/
          requires-signature bleeding_edge1
	  event "E3[rb] BLEEDING-EDGE Malware Windows executable sent from remote host"
	  tcp-state established
}
signature bleeding_edge1{
          ip-proto == tcp
          src-port != 20
          payload /.*This program cannot be run in DOS mode/
          tcp-state established
}

signature bothunter_malware{
          ip-proto == tcp
          src-port != 20
          payload /.*MZ/
          requires-signature bothunter_malware1
          tcp-state established
	  event "E3[rb] BotHunter Malware Windows executable (PE) sent from remote host"
}
signature bothunter_malware1{
          ip-proto == tcp
          src-port != 20
          payload /.*PE\x00 \x00/
          tcp-state established
}


signature bothunter_scrip{
          ip-proto == tcp
          payload /get/
          requires-signature bothunter_scrip2
          requires-signature bothunter_scrip3
          event "E3[rb] BotHunter HTTP-based .exe Upload on backdoor port"
}
signature bothunter_scrip2{
          ip-proto == tcp
          payload /echo/
}
signature bothunter_scrip3{
          ip-proto == tcp
          payload /.*.exe/
}

signature bothunter_http-based{
          ip-proto == tcp
	  dst-port >= 1030
	  dst-port <= 1040 
          payload /.*Content-Type: application x-exe/
	  event "E3[rb] BotHunter HTTP-based .exe Upload on backdoor port"
}

signature tftp_exe{
          ip-proto == udp
          dst-port == 69
          payload /(?= \x00 \x01)(?= .*.exe)/
          event "E3[rb] TFTP GET .exe from external source"
}

signature tftp_from{
          ip-proto == udp
          dst-port == 69
          payload /\x00 \x01/
          event "E3[rb] TFTP GET from external source"
}


signature et-worm_s{
          ip-proto == tcp
          dst-port == 9996
          payload /.*\x5F\x75\x70\x2E\x65\x78\x65/
          tcp-state established
	  event "E3[rb] ET WORM Sasser Transfer _up.exe"
}



