@load base/frameworks/notice

#notice 할 이름 선언
redef enum Notice::Type += {
      check4xx
};

#notice 에 적을 인포를 불러옴
event HTTP::log_http(rec: HTTP::Info){
      #notice를 작동시킬 조건문 4xx ~5xx 에러일 경우 작성
      if(rec$status_code>=400 && rec$status_code <=599){
      		#notice 정의. 안에 들어갈 내용들 연결 하기, 왼쪽 notice::info=채울내용들	      
      		NOTICE([$note=check4xx,
			$id=rec$id,
			$uid=rec$uid,
			$msg=fmt("%s error", rec$status_code),
			$sub=rec$status_msg
			]);	
      }
