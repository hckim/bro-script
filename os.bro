module os;
redef generate_OS_version_event: set[subnet]={172.16.0.0/16, 192.168.0.0/16, 10.10.0.0/16};

export{
redef enum Notice::Type += {os_found};
}

event OS_version_found(c:connection, host:addr, OS:OS_version)
{
NOTICE([$note=os_found, $conn=c,$msg=fmt("%s",OS)]);
}

event bro_init()
{
Log::add_filter(Notice::LOG, [$name="os-only", $path="osfound", $include=set("ts","id.orig_h","note","msg"), $pred(rec: Notice::Info) = {return rec$note == os_found;}]);

}
