#Outbound scan

signature registered_free_a{
	  ip-proto == tcp
	  src-port != 21, 22, 23
	  payload /(?=Microsoft Windows)(?=\x24c\x29 Copyright 1985-)(?=Microsoft Corp.)/ 
	  tcp-state established
	  event "E5[rb] REGISTERED FREE ATTACK-RESPONSES Microsoft cmd.exe banner"
}

signature p2p_edonkey{
          ip-proto == udp
	  src-port >= 1024
	  src-port <= 65535
	  dst-port >= 1024
          dst-port <= 65535
	  payload-size > 15
          payload /\xe3 \x0c/        
          event	"ET P2P Edonkey Publicize File"
}

signature p2p_edonkey2{
          ip-proto == udp
          src-port >= 1024
          src-port <= 65535
          dst-port >= 1024
          dst-port <= 65535
          payload-size > 19
          payload /\xe3 \x0c/
          event "ET P2P Edonkey Search Request (any type file)"
}
