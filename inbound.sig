#Inbound Exploit signature

signature shellcode_x86{
          ip-proto == tcp
	  tcp-state established
          payload /\x90 \x90 \x90 \x90 \x90 \x90 \x90 \x90 \x90 \x90/
          event "E2[rb] SHELLCODE x86 0x90 unicode NOOP"
}

signature bothunter_ex{
          ip-proto == tcp
          src-port == 445
          tcp-state established
          payload /3131313131313131313131313131313131313131313131/
          event "E2[rb] BotHunter EXPLOIT LSA exploit"

}

signature R_free_shellcode_x86{
          ip-proto = icmp
          #src-port = $shellcode-port #snort 에서 뭔지 알아봐야 함
          tcp-state established
          payload /CCCCCCCCCCCCCCCCCCCCCCCC/
          event "E2[rb] REGISTERED FREE SHELLCODE x86 inc ebx NOOP"
}

signature bleeding-edge{
          ip-proto == tcp
          src-port == 445
          tcp-state established
          payload /3131313131313131313131313131313131313131313131313131313131313131313131313131313131313131313131313131313131313131/
          event "E2[rb] BLEEDING-EDGE EXPLOIT LSA exploit"
}

signature chellcode_x86_2{
          ip-proto == tcp
          src-port == 445,1025
          src-port >= 135
          src-port <= 139
          tcp-state established
          payload /\x90 \x00 \x90 \x00 \x90 \x00 \x90 \x00 \x90 \x00/
          event "E2[rb] SHELLCODE x86 0x90 unicode NOOP port"
}

signature bleeding-edeg_w32{
          ip-proto == tcp
          tcp-state established
          payload /\x58 \xBC \x0C \xFF \x59 \x57 \x32 \x31 \xBD \xEC \x34 \x64 \x6E \xD6 \xE3 \x8D \x65 \x04 \x68 \x58 \x62 \x79 \xDF \xD8 \x2C \x25 \x6A \xB5 \x28 \xBA \x13 \x74/
          event "E2[rb] BLEEDING-EDGE VIRUS W32/Sasser.worm.b -NAI-)"
}

#Inbound Inbound

