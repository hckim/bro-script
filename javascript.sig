signature javascript_unescape{
  ip-proto == tcp
  tcp-state established, responder
  payload /.*[uU][nN][eE][sS][cC][aA][pP][eE]/
  event "ES|Javascript unescape() function detected."
}

signature javascript_eval{
  ip-proto == tcp
  tcp-state established, responder
  payload /.*[eE][vV][aA][lL]/
  event "ES|Javascript eval() function detected."
}

signature javascript_string_fromcharcode{
  ip-proto == tcp
  tcp-state established, responder
  payload /.*[sS][tT][rR][iI][nN][gG]\.[fF][rR][oO][mM][cC][hH][aA][rC][cC][oO][dD][eE]/
  event "ES|Javascript String.fromCharCode() function detected."
}

signature javascript_replace{
  ip-proto == tcp
  tcp-state established, responder
  payload /.*[rR][eE][pP][lL][aA][cC][eE]/
  event "ES|Javascript replace() function detected."
}

signature javascript_charcodeat{
  ip-proto == tcp
  tcp-state established, responder
  payload /.*[cC][hH][aA][rR][cC][oO][dD][eE][aA][tT]/
  event "ES|Javascript CharCodeAt() function detected."
}

signature javascript_substr{
  ip-proto == tcp
  tcp-state established, responder
  payload /.*[sS][uU][bB][sS][tT][rR]/
  event "ES|Javascript substr() function detected."
}
